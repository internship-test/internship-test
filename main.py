import os
import pysftp
from datetime import datetime
import logging

#for logging
logging.basicConfig(filename='delivery_logs.log', level=logging.INFO,format='%(asctime)s:%(levelname)s:%(message)s')

# SFTP credentials
sftp_host = 'x.x.x.x'
sftp_port = 22
sftp_username = 'example'
sftp_password = 'example'


filename = 'CompanyFile.csv'

incoming_dir = '/incoming'
processed_dir = '/processed'

# Connect to the SFTP server
with pysftp.Connection(sftp_host, username=sftp_username, password=sftp_password, port=sftp_port) as sftp:
     
    sftp.cwd(incoming_dir)

    # Check if the file exists in the incoming directory
    if filename in sftp.listdir():
        logging.info(f'{filename} is in the incoming directory.')
        #move the file
        sftp.rename(os.path.join(incoming_dir, filename), os.path.join(processed_dir, filename))
        logging.info(f'{filename} moved to the processed directory.')
    else:
        logging.error(f'{filename} is not in the incoming directory.')
