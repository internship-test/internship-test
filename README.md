## Prerequisites
- run the following command to install dependenies

```sh
pip install -r requirements.txt
```
The scripts assumes that you have a `SFTP` server running and `/incoming` and `/processed` folders exists in it's defualt directory, otherwise you can change the directory from line 18 and 19.

- After that fill in the `SFTP` credentials for the server in the code.

- you can then proceed to create a cron job that runs the script every day at 8:00 AM. the cronjob script for it is:
```sh
0 8 * * * /usr/bin/python /path/to/your/python/main.py
```
the cron job can be added via `crontab` tool; run `crontab -e` and then paste the cronjob above into the editor.

# Procedure for team members
read through the `delivery_logs` every day to see output of the cron job. you can see the processing logs marked with `INFO` while any exceptions or erros is marked with `ERROR`
